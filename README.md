# minigmp

Package minigmp is a small implementation of a subset of GMP's mpn and mpz interfaces.

Installation

    $ go get modernc.org/minigmp

Documentation: [godoc.org/modernc.org/minigmp](http://godoc.org/modernc.org/minigmp)
