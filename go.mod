module modernc.org/minigmp

require (
	github.com/remyoudompheng/bigfft v0.0.0-20170806203942-52369c62f446 // indirect
	modernc.org/ccgo v1.0.0
	modernc.org/ccir v0.0.0-20181106174718-5753a3f77739 // indirect
	modernc.org/internal v1.0.0 // indirect
	modernc.org/mathutil v1.0.0 // indirect
	modernc.org/memory v1.0.0 // indirect
)
